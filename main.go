package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
)

func main() {

	name := "NONE"

	if val, found := os.LookupEnv("SGHS_NAME"); found {
		name = val
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "SGHS[%v]: Hello, %q", name, html.EscapeString(r.URL.Path[1:]))
	})

	http.HandleFunc("/echo", func(w http.ResponseWriter, r *http.Request) {
		qParams := r.URL.Query()
		fmt.Fprintf(w, "SGHS[%v]: Echo Service\n", name)
		if len(qParams) > 0 {
			for k, v := range qParams {
				fmt.Fprintf(w, "Key [%v], Value [%v]!\n", k, v)
			}
		} else {
			fmt.Fprintf(w, "No query param!\n")
		}
	})

	log.Fatal(http.ListenAndServe(":80", nil))
}
